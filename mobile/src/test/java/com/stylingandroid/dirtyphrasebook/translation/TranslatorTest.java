package com.stylingandroid.dirtyphrasebook.translation;

import android.content.res.Resources;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class TranslatorTest {
    private static final String TEST_LANGUAGE = "test";
    private static final String[] TRANSLATIONS = {
            "One",
            "Two",
            "Three",
            "Four",
            "Five"
    };
    private Translator translator;

    @Before
    public void setup() {
        Resources resources = mock(Resources.class);
        when(resources.getStringArray(anyInt())).thenReturn(TRANSLATIONS);
        when(resources.getIdentifier(anyString(), anyString(), anyString())).thenReturn(1);
        Map<String, String> translationsMap = new HashMap<>(TRANSLATIONS.length);
        translationsMap.put(TEST_LANGUAGE, TEST_LANGUAGE);
        translator = new Translator(resources, null, translationsMap);
        translator.setLanguage(TEST_LANGUAGE);
    }

    @Test
    public void testUniformTranslation() {
        String translated = translator.getTranslation("Hello World!");
        assertEquals("Lowercase matches", translated, translator.getTranslation("hello world!"));
        assertEquals("Without punctuation matches", translated, translator.getTranslation("hello world"));
        assertEquals("Multiple spaces matches", translated, translator.getTranslation("  hello       world   "));
    }
}
