package com.stylingandroid.dirtyphrasebook;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.stylingandroid.dirtyphrasebook.translation.Translator;
import com.stylingandroid.dirtyphrasebook.tts.TextToSpeechCompat;
import com.stylingandroid.dirtyphrasebook.view.InputView;

import java.util.Arrays;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, InputView.InputListener {
    private static final String ALPHA = "alpha";
    private static final String SHARED_PREFERENCES_NAME = "com.stylingandroid.dirtyphrasebook";
    private static final String KEY_PHRASE = "KEY_PHRASE";
    private static final String KEY_LANGUAGE = "KEY_LANGUAGE";

    private ActionBar actionBar;
    private View container;
    private InputView inputView;
    private View translationPanel;
    private TextView translationLabel;
    private View translationTts;
    private TextView translation;
    private Spinner spinner;

    private SharedPreferences sharedPreferences;

    private Translator translator;
    private TextToSpeechCompat textToSpeech;

    private EndEditListener endEditListener = new EndEditListener();

    private boolean isTtsAvailable = false;
    private boolean isInputMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!BuildConfig.DEBUG) {
            Fabric.with(this, new Crashlytics());
        }
        setContentView(R.layout.activity_main);
        sharedPreferences = getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        translator = Translator.getInstance(this);
        String[] languages = getResources().getStringArray(R.array.readable_languages);

        findViews();
        setupToolbar();
        ArrayAdapter<String> adapter = setupLanguageSpinner(languages);
        setupCopy();
        restoreAppState(languages[0], adapter);

        inputView.registerInputListener(this);
        textToSpeech = TextToSpeechCompat.newInstance(this, new TtsInitListener());
        translationTts.setOnClickListener(new TtsCLickListener());
    }

    @Override
    protected void onDestroy() {
        textToSpeech.shutdown();
        inputView.unregisterInputListener(this);
        super.onDestroy();
    }

    private void findViews() {
        container = findViewById(R.id.layout_container);
        spinner = (Spinner) findViewById(R.id.language);
        inputView = (InputView) findViewById(R.id.input_view);
        translationPanel = findViewById(R.id.translation_panel);
        translationLabel = (TextView) findViewById(R.id.translation_label);
        translationTts = findViewById(R.id.translation_speak);
        translation = (TextView) findViewById(R.id.translation);
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }
    }

    private ArrayAdapter<String> setupLanguageSpinner(String[] languages) {
        Arrays.sort(languages);
        Context themedContext = actionBar != null ? actionBar.getThemedContext() : this;
        ArrayAdapter<String> adapter = new ArrayAdapter<>(themedContext, android.R.layout.simple_spinner_dropdown_item, languages);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
        return adapter;
    }

    private void setupCopy() {
        View translationCopy = findViewById(R.id.translation_copy);
        translationCopy.setOnLongClickListener(new MainActivity.CopyListener());
    }

    private void restoreAppState(String defaultLanguage, ArrayAdapter<String> adapter) {
        String language = sharedPreferences.getString(KEY_LANGUAGE, defaultLanguage);
        spinner.setSelection(adapter.getPosition(language));
        String phrase = sharedPreferences.getString(KEY_PHRASE, null);
        if (!TextUtils.isEmpty(phrase)) {
            inputView.setText(phrase);
            translationPanel.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        if (isInputMode) {
            inputView.endInputMode();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String language = spinner.getSelectedItem().toString();
        translator.setLanguage(language);
        updateTtsStatus(language);
        updateTranslation(inputView.getText());
        sharedPreferences.edit().putString(KEY_LANGUAGE, language).apply();
    }

    private void updateTtsStatus(String language) {
        Locale locale = translator.getLocaleForLanguage(language);
        if (isTtsAvailable && textToSpeech.isLanguageAvailable(locale)) {
            translationLabel.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_tts, 0, 0, 0);
            translationTts.setEnabled(true);
        } else {
            translationLabel.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            translationTts.setEnabled(false);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //NO-OP
    }

    @Override
    public void inputChanged(String input) {
        if (TextUtils.isEmpty(input)) {
            clearTranslation();
            sharedPreferences.edit().remove(KEY_PHRASE).apply();
        } else {
            updateTranslation(input);
            sharedPreferences.edit().putString(KEY_PHRASE, input).apply();
        }
    }

    @Override
    public Animator startInputMode() {
        isInputMode = true;
        container.setOnClickListener(endEditListener);
        return getHideTranslationAnimator();
    }

    private Animator getHideTranslationAnimator() {
        Animator animator = ObjectAnimator.ofFloat(translationPanel, ALPHA, 1f, 0f);
        animator.addListener(new HideAnimationListener());
        return animator;
    }

    @Override
    public Animator endInputMode(boolean hasValue) {
        isInputMode = false;
        container.setOnClickListener(null);
        return getShowTranslationAnimator(hasValue);
    }

    private Animator getShowTranslationAnimator(boolean hasValue) {
        translationPanel.setVisibility(View.VISIBLE);
        return ObjectAnimator.ofFloat(translationPanel, ALPHA, translationPanel.getAlpha(), hasValue ? 1f : 0f);
    }

    private void updateTranslation(String input) {
        translationLabel.setText(spinner.getSelectedItem().toString());
        translation.setText(translator.getTranslation(input));
    }

    private void clearTranslation() {
        Animator animator = getHideTranslationAnimator();
        animator.setDuration(inputView.getAnimationDuration());
        animator.start();
    }

    private class HideAnimationListener extends AnimatorListenerAdapter {
        @Override
        public void onAnimationEnd(@NonNull Animator animation) {
            super.onAnimationEnd(animation);
            translationPanel.setVisibility(View.INVISIBLE);
        }
    }

    private class TtsCLickListener implements View.OnClickListener {

        @Override
        public void onClick(@NonNull View v) {
            if (isTtsAvailable) {
                String language = spinner.getSelectedItem().toString();
                Locale locale = translator.getLocaleForLanguage(language);
                if (textToSpeech.isLanguageAvailable(locale)) {
                    textToSpeech.setLanguage(locale);
                } else {
                    textToSpeech.setLanguage(Locale.getDefault());
                }
                textToSpeech.speak(translation.getText().toString(), TextToSpeech.QUEUE_FLUSH, 1f);
            }
        }
    }

    private class TtsInitListener implements TextToSpeech.OnInitListener {

        @Override
        public void onInit(int status) {
            if (status == TextToSpeech.SUCCESS) {
                isTtsAvailable = true;
                String language = spinner.getSelectedItem().toString();
                updateTtsStatus(language);
            }
        }
    }

    private class CopyListener implements View.OnLongClickListener {
        @Override
        public boolean onLongClick(@NonNull View v) {
            ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            clipboardManager.setPrimaryClip(ClipData.newPlainText(getString(R.string.app_name), translation.getText()));
            Toast.makeText(MainActivity.this, getString(R.string.text_compied), Toast.LENGTH_LONG).show();
            return true;
        }
    }

    private class EndEditListener implements View.OnClickListener {
        @Override
        public void onClick(@NonNull View v) {
            inputView.endInputMode();
        }
    }
}
