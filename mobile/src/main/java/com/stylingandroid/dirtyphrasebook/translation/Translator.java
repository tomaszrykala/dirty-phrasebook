package com.stylingandroid.dirtyphrasebook.translation;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.SparseArray;

import com.stylingandroid.dirtyphrasebook.DeveloperException;
import com.stylingandroid.dirtyphrasebook.R;

import java.util.Collection;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Translator {
    private static final String RESOURCE_TYPE = "array";
    private static final Pattern LANGUAGE_COUNTRY_PATTERN = Pattern.compile("(.*?)_(.*?)");

    private final Resources resources;
    private final String packageName;
    private final Map<String, String> languagesMap;
    private final SparseArray<Integer> hashLookup = new SparseArray<>();

    private String[] translations;
    private Locale defaultLocale = Locale.getDefault();

    Translator(Resources resources, String packageName, Map<String, String> languagesMap) {
        this.resources = resources;
        this.packageName = packageName;
        this.languagesMap = languagesMap;
    }

    public static Translator getInstance(Context context) {
        Resources resources = context.getResources();
        String packageName = context.getPackageName();
        String[] languages = resources.getStringArray(R.array.languages);
        String[] readableLanguages = resources.getStringArray(R.array.readable_languages);
        if (languages.length != readableLanguages.length) {
            throw new DeveloperException("R.array.languages and R.array.readable_languages need to be the same size. Bad developer!");
        }
        Map<String, String> languagesMap = new HashMap<>(readableLanguages.length);
        for (int index = 0; index < readableLanguages.length; index++) {
            languagesMap.put(readableLanguages[index], languages[index]);
        }
        Translator translator = new Translator(resources, packageName, languagesMap);
        translator.updateHashLookup();
        return translator;
    }

    public void setLanguage(String language) {
        translations = getTranslations(languagesMap.get(language));
    }

    public String getTranslation(String source) {
        String sanitised = StringSanitser.sanitise(source);
        int hashcode = sanitised.hashCode();
        Integer index = hashLookup.get(hashcode);
        if (index == null) {
            index = Math.abs(hashcode % translations.length);
        }
        return translations[index];
    }

    private String[] getTranslations(String language) {
        final int resourceId = resources.getIdentifier(language, RESOURCE_TYPE, packageName);
        if (resourceId == 0) {
            throw new DeveloperException("The dev is cray cray. Cannot find string array for language " + language);
        }
        return resources.getStringArray(resourceId);
    }

    private void updateHashLookup() {
        Collection<String> languages = languagesMap.values();
        hashLookup.clear();
        for (String language : languages) {
            String[] allTranslations = getTranslations(language);
            for (int index = 0; index < allTranslations.length; index++) {
                String sanitised = StringSanitser.sanitise(allTranslations[index]);
                int sanitisedHashCode = sanitised.hashCode();
                if (hashLookup.get(sanitisedHashCode) != null) {
                    throw new DeveloperException("Dude, we have duplicate hash codes for translations strings");
                }
                hashLookup.put(sanitisedHashCode, index);
            }
        }
    }

    public Locale getLocaleForLanguage(String readableLanguage) {
        String language = languagesMap.get(readableLanguage);
        String country = null;
        Matcher matcher = LANGUAGE_COUNTRY_PATTERN.matcher(language);
        if (matcher.matches()) {
            language = matcher.group(1);
            country = matcher.group(2);
        }
        if (language.equals(defaultLocale.getLanguage())) {
            return defaultLocale;
        }
        return TextUtils.isEmpty(country) ? new Locale(language) : new Locale(language, country);
    }
}
