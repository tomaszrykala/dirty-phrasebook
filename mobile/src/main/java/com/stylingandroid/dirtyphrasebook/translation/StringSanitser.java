package com.stylingandroid.dirtyphrasebook.translation;

import java.text.Normalizer;
import java.util.Locale;
import java.util.regex.Pattern;

public final class StringSanitser {
    private static final String EMPTY_STRING = "";
    private static final String SPACE = " ";
    private static final String NON_LETTERS = "\\p{Punct}";
    private static final String ACCENTS_PATTERN_STRING = "\\p{M}";
    private static final String MULTIPLE_SPACES_PATTERN_STRING = "\\s{2,}";
    private static final Pattern ACCENTS_PATTERN = Pattern.compile(ACCENTS_PATTERN_STRING);
    private static final Pattern MULTIPLE_SPACES_PATTERN = Pattern.compile(MULTIPLE_SPACES_PATTERN_STRING);

    private StringSanitser() {
        //NO-OP
    }

    public static String sanitise(String string) {
        String sanitised = normaliseCase(string);
        sanitised = removeDiacritics(sanitised);
        sanitised = removeNonLetters(sanitised);
        sanitised = removeMultipleSpaces(sanitised);
        return sanitised.trim();
    }

    static String normaliseCase(String string) {
        return string.trim().toLowerCase(Locale.getDefault());
    }

    static String removeNonLetters(String string) {
        return string.replaceAll(NON_LETTERS, EMPTY_STRING);
    }

    static String removeDiacritics(String string) {
        String normalised = Normalizer.normalize(string, Normalizer.Form.NFD);
        return ACCENTS_PATTERN.matcher(normalised).replaceAll(EMPTY_STRING);
    }

    static String removeMultipleSpaces(String string) {
        return MULTIPLE_SPACES_PATTERN.matcher(string).replaceAll(SPACE);
    }
}
