package com.stylingandroid.dirtyphrasebook.view;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.stylingandroid.dirtyphrasebook.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InputView extends CardView {
    private static final String EMPTY_STRING = "";
    public static final String TRANSLATION_Y = "TranslationY";
    public static final String ALPHA = "alpha";
    public static final int SWIPE_DIRECTION_FACTOR = 5;

    private EditText input;
    private View clearInput;
    private View inputDone;
    private View focusHolder;

    private GestureDetectorCompat gestureDetector;

    private float offsetHeight;
    private int duration;

    private Set<InputListener> listeners = new HashSet<>();

    public InputView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialise(context, attrs, 0);
    }

    public InputView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialise(context, attrs, defStyleAttr);
    }

    private void initialise(Context context, AttributeSet attrs, int defStyleAttr) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.InputView, defStyleAttr, 0);
        offsetHeight = a.getDimension(R.styleable.InputView_offset, 0f);
        duration = a.getInteger(R.styleable.InputView_duration, 0);
        a.recycle();
        gestureDetector = new GestureDetectorCompat(context, new GestureListener());
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        LayoutInflater inflater = LayoutInflater.from(getContext());
        inflater.inflate(R.layout.input_view, this, true);
        input = (EditText) findViewById(R.id.input);
        focusHolder = findViewById(R.id.focus_holder);
        clearInput = findViewById(R.id.clear_input);
        inputDone = findViewById(R.id.input_done);

        input.setOnFocusChangeListener(new FocusChangeListener());
        input.setOnEditorActionListener(new InputCompleteListener());
        input.addTextChangedListener(new InputChangeListener());
        input.setOnTouchListener(new InputOnTouchListener());
        clearInput.setOnClickListener(new ClearListener());
        inputDone.setOnClickListener(new DoneListener());
    }

    public void registerInputListener(InputListener listener) {
        listeners.add(listener);
    }

    public void unregisterInputListener(InputListener listener) {
        listeners.remove(listener);
    }

    private void startInputMode() {
        AnimatorSet animatorSet = new AnimatorSet();
        List<Animator> animators = new ArrayList<>();
        inputDone.setVisibility(VISIBLE);
        animators.add(ObjectAnimator.ofFloat(this, TRANSLATION_Y, 0, -offsetHeight));
        animators.add(ObjectAnimator.ofFloat(inputDone, ALPHA, 0f, 1f));
        for (InputListener listener : listeners) {
            animators.add(listener.startInputMode());
        }
        animatorSet.playTogether(animators);
        animatorSet.setDuration(duration);
        animatorSet.start();
    }

    public void endInputMode() {
        focusHolder.requestFocus();
        AnimatorSet myAnimators = new AnimatorSet();
        AnimatorSet listenerAnimators = new AnimatorSet();
        myAnimators.playTogether(ObjectAnimator.ofFloat(this, TRANSLATION_Y, -offsetHeight, 0),
                ObjectAnimator.ofFloat(inputDone, ALPHA, 1f, 0f));
        myAnimators.setDuration(duration);
        List<Animator> animators = new ArrayList<>();
        for (InputListener listener : listeners) {
            animators.add(listener.endInputMode(!TextUtils.isEmpty(input.getText())));
        }
        listenerAnimators.playTogether(animators);
        listenerAnimators.setDuration(duration);
        myAnimators.addListener(new EndInputAnimationListener());
        AnimatorSet sequence = new AnimatorSet();
        sequence.playSequentially(myAnimators, listenerAnimators);
        sequence.start();
    }

    private void inputChanged(String inputString) {
        for (InputListener listener : listeners) {
            listener.inputChanged(inputString);
        }
    }

    public String getText() {
        return input.getText().toString();
    }

    public void setText(CharSequence text) {
        input.setText(text);
    }

    public int getAnimationDuration() {
        return duration;
    }

    public interface InputListener {
        void inputChanged(String input);

        Animator startInputMode();

        Animator endInputMode(boolean hasValue);
    }

    private class FocusChangeListener implements View.OnFocusChangeListener {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (v.equals(input)) {
                if (hasFocus) {
                    startInputMode();
                } else {
                    endInputMode();
                }
            }
        }
    }

    private class InputCompleteListener implements TextView.OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                endInputMode();
                return true;
            }
            return false;
        }
    }

    private class InputOnTouchListener implements OnTouchListener {
        @Override
        public boolean onTouch(View v, @NonNull MotionEvent event) {
            return inputDone.getVisibility() == VISIBLE && gestureDetector.onTouchEvent(event);
        }
    }

    private class EndInputAnimationListener extends AnimatorListenerAdapter {
        @Override
        public void onAnimationEnd(@NonNull Animator animation) {
            super.onAnimationEnd(animation);
            InputMethodManager imm = (InputMethodManager) getContext().getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
            inputChanged(getText());
            inputDone.setVisibility(INVISIBLE);
        }
    }

    private class InputChangeListener implements android.text.TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //NO-OP
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            //NO-OP
        }

        @Override
        public void afterTextChanged(Editable s) {
            clearInput.setVisibility(TextUtils.isEmpty(s) ? View.INVISIBLE : View.VISIBLE);
        }
    }

    private class ClearListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            input.setText(EMPTY_STRING);
            inputChanged(EMPTY_STRING);
        }
    }

    private class DoneListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            endInputMode();
        }
    }

    private final class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            if (isSwipeDown(velocityX, velocityY)) {
                endInputMode();
            }
            return false;
        }

        private boolean isSwipeDown(float velocityX, float velocityY) {
            return velocityY > 0 && velocityY > velocityX * SWIPE_DIRECTION_FACTOR;
        }
    }

}
